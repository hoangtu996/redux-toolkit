import axios ,{ AxiosInstance} from "axios";

class Http {
    instance: AxiosInstance;
    constructor() {
        this.instance = axios.create({
            baseURL: "http://localhost:3304",
            timeout: 1000
        })
    }
}
const http = new Http().instance
export default http;