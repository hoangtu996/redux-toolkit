import { Post } from "pages/types/blog.type";
import { useEffect, useState } from "react";
import { toast } from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';
interface PostItemType {
  post: Post;
  handelDelete: (postId: string) => void,
  handleEditingPost: (postId: string) => void
}
export default function PostItem({ post, handelDelete, handleEditingPost }: PostItemType) {

  const [showFromConfirm, setShowFromConfirm] = useState(false)
  const handleConfirmDelete = () => {
    Promise
      .resolve(handelDelete(post.id))
      .then(() => {
        toast.success('Xóa bài viết thành công !')
      })
      .catch((error) => {
        console.log(error)
        toast.error('Xóa bài viết thất bại')
      })
  }
  const handleEditClicks = (postId: string) => {
    handleEditingPost(postId)
    // roll top
    window.scrollTo({
      top: 0,
      behavior: "smooth",
    });
  }
  return (
    <div className="flex flex-col items-center overflow-hidden rounded-lg border md:flex-row">
      <div className="group relative block h-48 w-full shrink-0 self-start overflow-hidden bg-gray-100 md:h-full md:w-32 lg:w-48">
        <img
          src={post.featuredImage}
          loading="lazy"
          alt={post.title}
          className="absolute inset-0 h-full w-full object-cover object-center transition duration-200 group-hover:scale-110"
        />
      </div>
      <div className="flex flex-col gap-2 p-4 lg:p-6">
        <span className="text-sm text-gray-400">{post.publishDate}</span>
        <h2 className="text-xl font-bold text-gray-800">{post.title}</h2>
        <p className="text-gray-500">{post.description}</p>
        <div>
          <div className="inline-flex rounded-md shadow-sm" role="group">
            <button
              type="button"
              className="rounded-l-lg border border-gray-200 bg-white px-4 py-2 text-sm font-medium text-gray-900 hover:bg-gray-100 hover:text-blue-700 focus:z-10 focus:text-blue-700 focus:ring-2 focus:ring-blue-700"
              onClick={() => handleEditClicks(post.id)}
            >
              Edit
            </button>
            <button
              type="button"
              className="rounded-r-lg border-b border-r border-t border-gray-200 bg-white px-4 py-2 text-sm font-medium text-gray-900 hover:bg-gray-100 hover:text-blue-700 focus:z-10 focus:text-blue-700 focus:ring-2 focus:ring-blue-700"
              onClick={() => setShowFromConfirm(true)}
            >
              Delete
            </button>
          </div>
        </div>
      </div>
      {
        showFromConfirm && (
          <div className="fixed inset-0 bg-gray-800 bg-opacity-50 flex justify-center items-center z-10">
            <div className="bg-white p-6 rounded-md">
              <h2 className="text-lg font-bold mb-4">Confirm Delete</h2>
              <p>Are you sure you want to delete this post?</p>
              <div className="mt-4 flex justify-end gap-2">
                <button
                  className="bg-red-500 hover:bg-red-600 px-4 py-2 rounded-md text-white"
                  onClick={handleConfirmDelete}
                >
                  Yes
                </button>
                <button
                  className="bg-gray-200 hover:bg-gray-300 px-4 py-2 rounded-md"
                  onClick={() => setShowFromConfirm(false)}
                >
                  No
                </button>
              </div>
            </div>
          </div>
        )
      }
    </div>
  );
}
