"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var _a;
exports.__esModule = true;
exports.updatingPost = exports.canclePost = exports.editingPost = exports.deletePost = exports.addPost = exports.getPostsList = void 0;
var toolkit_1 = require("@reduxjs/toolkit");
var nanoid_1 = require("nanoid");
var http_1 = require("utils/http");
// custom nanoid implementation
var nanoid = nanoid_1.customAlphabet('ABCDEF1234567890', 12);
//  declare state of Blog
var initialState = {
    postList: [],
    editingPost: null
};
exports.getPostsList = toolkit_1.createAsyncThunk('blog/getPostsList', function (_, thunkApi) { return __awaiter(void 0, void 0, void 0, function () {
    var response;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, http_1["default"].get('posts', {
                    signal: thunkApi.signal
                })];
            case 1:
                response = _a.sent();
                return [2 /*return*/, response.data];
        }
    });
}); });
exports.addPost = toolkit_1.createAsyncThunk('blog/addPostList', function (_, thunkApi) { return __awaiter(void 0, void 0, void 0, function () {
    return __generator(this, function (_a) {
        return [2 /*return*/];
    });
}); });
// Using createSlice
// Create a slice using Redux Toolkit
var blogReducer = toolkit_1.createSlice({
    name: "blog",
    initialState: initialState,
    reducers: {
        addPost: function (state, action) {
            var post = action.payload;
            state.postList.push(__assign(__assign({}, post), { id: nanoid() }));
        },
        deletePost: function (state, action) {
            var postId = action.payload;
            var findIndex = state.postList.findIndex(function (post) { return post.id === postId; });
            if (findIndex !== -1) {
                state.postList.splice(findIndex, 1);
            }
        },
        editingPost: function (state, action) {
            var postId = action.payload;
            var findPost = state.postList.find(function (post) { return post.id === postId; }) || null;
            state.editingPost = findPost;
        },
        canclePost: function (state) {
            state.editingPost = null;
        },
        updatingPost: function (state, action) {
            var postId = action.payload.id;
            var updatedPost = action.payload;
            var index = state.postList.findIndex(function (post) { return post.id === postId; });
            if (index !== -1) {
                state.postList[index] = updatedPost;
                state.editingPost = null;
            }
        }
    },
    extraReducers: function (builder) {
        builder
            .addCase(exports.getPostsList.fulfilled, function (state, action) {
            state.postList = action.payload;
        })
            .addMatcher(function (action) { return action.type.includes('cancle'); }, function (state, action) {
            console.log(toolkit_1.current(state));
        }).addDefaultCase(function (state, action) {
            console.log("action type:  " + action.type, toolkit_1.current(state));
        });
    }
});
// Extract the actions and reducer from the slice
exports.deletePost = (_a = blogReducer.actions, _a.deletePost), exports.editingPost = _a.editingPost, exports.canclePost = _a.canclePost, exports.updatingPost = _a.updatingPost;
exports["default"] = blogReducer.reducer;
