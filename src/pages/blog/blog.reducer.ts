import { initalPostList } from 'constants/blog';
import { Post } from '../types/blog.type';
import { AsyncThunk, PayloadAction, createAction, createAsyncThunk, createReducer, createSlice, current } from "@reduxjs/toolkit"
import { customAlphabet } from 'nanoid';
import http from 'utils/http';

type GenericAsyncThunk = AsyncThunk<unknown, unknown, any>
type PendingAction = ReturnType<GenericAsyncThunk['pending']>
type RejectedAction = ReturnType<GenericAsyncThunk['rejected']>
type FulfilledAction = ReturnType<GenericAsyncThunk['fulfilled']>
// declare Post interface
interface BlogState {
  postList: Post[],
  editingPost: Post | null,
  loading: boolean,
  currentRequestId: undefined | string
}
// custom nanoid implementation
const nanoid = customAlphabet('ABCDEF1234567890', 12)
//  declare state of Blog
const initialState: BlogState = {
  postList: [],
  editingPost: null,
  loading: false,
  currentRequestId: undefined
}
export const getPostsList = createAsyncThunk('blog/getPost', async (_, thunkApi) => {
  const response = await http.get<Post[]>('posts', {
    signal: thunkApi.signal
  })
  return response.data
})

export const addPost = createAsyncThunk('blog/addPost', async (body: Omit<Post, 'id'>, thunkApi) => {
  const response = await http.post<Post>('posts', body, {
    signal: thunkApi.signal
  })
  return response.data
})

export const updatePost = createAsyncThunk(
  'blog/updatePost',
  async ({ postId, body }: { postId: string, body: Post }, thunkApi) => {
    const response = await http.put<Post>(`posts/${postId}`, body, {
      signal: thunkApi.signal
    })
    return response.data
  })

export const deletePost = createAsyncThunk(
  'blog/deletePost',
  async (postId: string, thunkApi) => {
    const response = await http.delete<Post>(`posts/${postId}`, {
      signal: thunkApi.signal
    })
    return response.data
  })

// Using createSlice
// Create a slice using Redux Toolkit
const blogReducer = createSlice({
  name: "blog",
  initialState,
  reducers: {
    editingPost: (state, action: PayloadAction<string>) => {
      const postId = action.payload;
      const findPost = state.postList.find((post) => post.id === postId) || null;
      state.editingPost = findPost;
    },
    canclePost: (state) => {
      state.editingPost = null;
    },
  },
  extraReducers(builder) {
    builder
      .addCase(getPostsList.fulfilled, (state, action) => {
        state.postList = action.payload
      })
      .addCase(addPost.fulfilled, (state, action) => {
        state.postList.push(action.payload);
      })
      .addCase(updatePost.fulfilled, (state, action) => {
        state.postList.find((post, index) => {
          if (post.id === action.payload.id) {
            state.postList[index] = action.payload
            return true;
          }
          return false;
        })
      })
      .addCase(deletePost.fulfilled, (state, action) => {
        const postId = action.meta.arg
        const findIndex = state.postList.findIndex(post => post.id === postId)
        if (findIndex !== -1) {
          state.postList.splice(findIndex, 1);
        }
      })
      .addMatcher<PendingAction>(
        (action) => action.type.endsWith('/pending'),
        (state, action) => {
          state.loading = true;
          state.currentRequestId = action.meta.requestId;
        }
      ).addMatcher<RejectedAction>(
        (action) => action.type.endsWith('/rejected'),
        (state, action) => {
          if(state.loading && state.currentRequestId === action.meta.requestId ){
            state.loading = false;
            state.currentRequestId = undefined;
          }
        
        }
      ).addMatcher<FulfilledAction>(
        (action) => action.type.endsWith('/fulfilled'),
        (state, action) => {
          if(state.loading && state.currentRequestId === action.meta.requestId){
            state.loading = false;
            state.currentRequestId = undefined;
          }
        
        }
      )
      .addDefaultCase((state, action) => {
        console.log(`action type:  ${action.type}`, current(state));
      })
  }
});

// Extract the actions and reducer from the slice
export const { editingPost, canclePost } = blogReducer.actions
export default blogReducer.reducer